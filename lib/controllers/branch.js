var Branch = require('../models/branch');
var errorHelper = require('mongoose-error-helper').errorHelper;

exports.createBranch = function(req, res){
    var branch = new Branch(req.body);
    branch.save(function(error, branch){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({branch: branch});
        }
    });
}

exports.getBranch = function(req, res){
    Branch.findById(req.params.id, function(error, branch){
        if(err){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({branch: branch});
        }
    });
}

exports.editBranch = function(req, res){
    Branch.findByIdAndUpdate(req.params.id, {$set: req.body}, function(error, br){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({branch: br});
        }
    });
}

exports.deleteBranch = function(req, res){
    Branch.findByIdAndUpdate(req.body.id, {$set: {state: 'deactivated'}}, function(error, br){
        if(error){
            return res.json({error: errorHelper(error)});
        }else{
            return res.json({branch: br});
        }
    });
}

exports.listBranches = function(req, res){
    var page_count = 1;
    var post_per_page = 20;

    if(req.params.page){
        page_count = parseInt(req.params.page);
//            console.log(req.params.page);
    }

    Branch
        .paginate({state: 'active'}, page_count, post_per_page, function(error, pageCount, results, itemCount){
            if(error){
                return res.json({error: errorHelper(error)});
            }else{
                return res.json({branches: results, pagecount: pageCount, itemcount: itemCount});
            }
        });
}