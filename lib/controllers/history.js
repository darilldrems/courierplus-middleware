var ShipmentHistory = require('../models/shipment_history');
var SealHistory = require('../models/seal_history');
var ShuttleHistory = require('../models/shuttle_history');

require('datejs');

exports.shipment_history_list = function(req, res){

    var condition = {};
    var page = 1;
    var total_per_page = 10;

//    console.log('raw data'+req.query.date);
//    console.log('raw date param'+req.params.date);
    console.log('parsed date'+Date.parse(req.query.date));

    if(req.query.date || req.query.hawb){
        condition = {
            date: {$gt: Date.parse(req.query.date)},
            hawb: req.query.hawb
        }
    }

    if(req.query.page)
        page = req.query.page;

    ShipmentHistory.paginate(condition, page, total_per_page, function(error, pageCount, paginatedResults, itemCount){
        if(error){
            return res.json({error: error});
        }else{
            return res.json({
                results: paginatedResults,
                page_count: pageCount,
                item_count: itemCount
            })
        }
    }, {populate: 'edited_by new_warehouse old_warehouse'});


}

exports.seal_history_list = function(req, res){
    var page = 1;

    var total_per_page = 10;

    var condition = {}

    if(req.query.page)
        page = req.query.page;

    if(req.query.date || req.query.number){
        condition = {
            date: {$gt: Date.parse(req.query.date)},
            number: req.query.number
        }
    }

    SealHistory.paginate(condition, page, total_per_page, function(error, pageCount, paginatedResults, itemCount){
        if(error){
            return res.json({error: error});
        }else{
            return res.json({
                item_count: itemCount,
                page_count: pageCount,
                results: paginatedResults
            })
        }
    }, {populate: 'edited_by'})


}

exports.shuttle_history_list = function(req, res){
    var page = 1;

    var total_per_page = 10;

    var condition = {}

    if(req.query.page)
        page = req.query.page;

    if(req.query.date || req.query.number){
        condition = {
            date: {$gt: Date.parse(req.query.date)},
            number: req.query.number
        }
    }

    ShuttleHistory.paginate(condition, page, total_per_page, function(error, pageCount, paginatedResults, itemCount){
        if(error){
            return res.json({error: error});
        }else{
            return res.json({
                item_count: itemCount,
                page_count: pageCount,
                results: paginatedResults
            })
        }
    }, {populate: 'edited_by'})
}