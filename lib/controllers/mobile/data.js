var Shipper = require('../../models/shipper');
var Branch = require('../../models/branch');

exports.shippers = function(req, res){
    Shipper.find({state: "active"}, function(error, results){
        if(error){
            return res.json({error: {message: "some errors occured"}});
        }else{
            var shippers = [];
            for(var i = 0; i < results.length; i++){
                var shipper = {
                    id: results[i]._id,
                    name: results[i].name
                };
//                shipper.id = results[i]._id;
                shippers.push(shipper);
            }
            return res.json(shippers);
        }
    })
}

exports.locations = function(req, res){

    Branch.find({}, function(err, results){
        if(err){
            return res.json({error: "Unable to fetch the branch location details"});
        }else{
            return res.json(results);
        }
    })

}
