var Shipment = require('../../models/shipment');
var authentication = require('../../helpers/authentication');


exports.delivery = function(req, res){
    var user_id = req.body.user_id;

    authentication.userWithIdExist(user_id, function(result){
        if(result.success){
            Shipment.findOne({hawb: req.body.hawb}, function(err, shipment){
                if(!err){
                    shipment.receiver_name = req.body.receiver_name;
                    shipment.receiver_tel = req.body.receiver_tel;
                    shipment.status = "Delivered";
                    shipment.amount_received = req.body.amount_received;
		    shipment.last_modified = Date.now();
                    if(req.body.payment_mode){
                        shipment.payment_mode = req.body.payment_mode;
                    }
                    shipment.save(function(err, saved){
                        if(err){
                            console.log("delivery failed");
                            return res.json("failed");
                        }else{
                            console.log("delivery successful");
                            return res.json("success");
                        }
                    })
                }else{
                    console.log("hawb does not exist");
                    return res.json({error: {message: 'Hawb not existing'}});
                }
            })
        }else{
            return res.json({error: {message: 'Unauthorized user'}});
        }
    })

}
