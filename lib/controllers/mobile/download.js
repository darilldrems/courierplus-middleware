var Shipment = require('../../models/shipment');
var md5 = require('MD5');
var authentication = require('../../helpers/authentication');
var request = require('request');

exports.download = function(req, res){

    var user_id = req.body.user_id;

    var api_url = "http://154.117.64.90:8080/middleware-api/api/get_shipments.php";
    authentication.userWithIdExist(user_id, function(result){
        if(result.success){

            var hawbs = req.body.hawbs;

            var hawbs_array = hawbs.split(',');
            
            var results = [];
            
            console.log("hawbs:"+hawbs_array);
            
            request.post({url: api_url, formData:{hawbs: hawbs_array}}, function(err, resp, body){
                if(err){
                    console.log("error:"+err);
                    return res.json({error: {message: "Error occured on server"}});
                }
                
                var shipments = JSON.parse(body);
                
                console.log("body:"+body);
                
                console.log("shipments:"+JSON.stringify(shipments));
                
                for(var i in shipments){
                    var shipment = shipments[i];
                    
                    var item = {
                            hawb: shipment.hawb,
                            pick_up_by: "",
                            payment_mode: "",
                            remark: "",
                            consignee_address: shipment.consignee_address,
                            consignee_tel: shipment.consignee_telephone,
                            consignee_name: shipment.consignee_name,
                            status: shipment.status,
                            content: shipment.content,
                            id: shipment.hawb,
                            shipper: {
                                name: shipment.shipper_name,
                                id: shipment.shipper_code
                            }
                        
                    }
                        
                  results.push(item);      
                }
                    
                    console.log("results:"+results);
                    return res.json(results);
                
                
            });


             
        }else{
            return res.json({error: {message: 'Unauthorized user'}});  
        }
    })




    

}