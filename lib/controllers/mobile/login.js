var md5 = require('MD5');
var authentication = require('../../helpers/authentication');

exports.login = function(req, res){

    var username = req.body.username;
    var password = md5(req.body.password);


    authentication.userExist(username, password, function(result){
        if(result.success && result.user.state === 'active'){

            var logged_in_user = {
                id: result.user._id,
                role: result.user.role || '',
                username: result.user.username,
                first_name: result.user.first_name || '',
                last_name: result.user.last_name || '',
                created_on: result.user.created_on || '',
                status: result.user.status || '',
                email: result.user.email || ''
            }
            return res.json(logged_in_user)
        }else{
            return res.json({error: {message: result.message}})
        }
    })


}