var Shipment = require('../../models/shipment');
var Seal = require('../../models/seal');
var md5 = require('MD5');
var authentication = require('../../helpers/authentication');

exports.seal = function(req, res){
    var user_id = req.body.user_id

    authentication.userWithIdExist(user_id, function(result){
        if(result.success){
            var seal = new Seal();
                seal.number = req.body.number;
                seal.departure = req.body.departure;
                seal.destination = req.body.destination;
                seal.shipments = req.body.shipments.split(',');
                seal.location = req.body.departure;

                seal.save(function(error, saved_seal){
                    if(error){
//                        TODO: find out if it is unique key error or not
                        if(error.code === 11000){
                            return res.json({error: {message: 'Duplicate seal number error. Please check seal number.'}});
                        }else{
                            console.log(error);
                            return res.json({error: {message: 'Some error occured. Please try again later'}});
                        }
                    }else{
                        return res.json(saved_seal);
                    }
                })

        }else{
            return res.json({error: {message: 'Unauthorized user'}});
        }
    })
}