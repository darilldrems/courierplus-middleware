var ControlShuttle = require('../../models/controlshuttle');
var Seal = require('../../models/seal');

exports.cs = function(req, res){

    ControlShuttle.find({$where: "this.destination != this.location"}).exec(function(err, result){

        if(err){
            return res.json({error: "Error on the server"});
        }else{
            return res.json(result);
        }

    });

}

exports.seal = function(req, res){
    Seal.find({$where: "this.destination != this.location"}).exec(function(err, result){

        if(err){
            return res.json({error: "Error on the server"});
        }else{
            return res.json(result);
        }

    });
}