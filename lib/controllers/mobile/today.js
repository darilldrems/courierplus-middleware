var Shipment = require('../../models/shipment');
var moment = require('moment');
exports.shipments = function(req, res){
	var today = moment().startOf('day');
	Shipment.find({last_modified: {$gt:today}}).exec(function(err, result){
		if(err){
			return res.json({error:err});
		}else{
			return res.json(result);
		}
	})
}
