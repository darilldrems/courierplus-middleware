/*
* list the different model list and enums to be use as filter options in the front end
* */

var Shipper = require('../models/shipper');
var Shipment = require('../models/shipment');
var _ = require('underscore');

exports.shipper_list  = function(req, res){

    Shipper.find({state: 'active'}, function(err, result){
        if(err){
            return res.json({error: err});
        }else{
            var shippers_by_name = [];
            _.each(result, function(obj){
                shippers_by_name.push(obj.name);
            })


            return res.json({shippers: shippers_by_name});
        }
    })


}


exports.content_types = function(req, res){
    res.json({
        content_types: Shipment.schema.path('content').enumValues
    });
}



exports.delivery_status = function(req, res){
    res.json({
        status: Shipment.schema.path('status').enumValues
    });

}