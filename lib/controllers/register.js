/**
 * Created by ridwan on 1/28/15.
 */
/**
 * Created by ridwan on 1/28/15.
 */

var User = require('../models/user');
var Auth = require('../models/auth')
var md5 = require('MD5');
var errorHelper = require('mongoose-error-helper').errorHelper;

exports.register = function(req, res){
        var u = new User({
            username: req.body.username,
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            role: req.body.role

        });

        u.save(function(err, user){
            if(!err){
                var auth = new Auth({
                    user: user._id,
                    password: md5(req.body.password)

                });

                auth.save(function(error, au){
                    if(error){
                        return res.json({error: errorHelper(error)});
                    }else{
                        return res.json({user_id: au._id});
                    }
                })

            }else{

                return res.json({error: errorHelper(err)});
            }
        })
}

exports.listAuth = function(req, res){
    Auth.find({}, function(err, a){
        if(!err){
            return res.json({date: a});
        }
    })
}
