var User = require('../models/user');
var Shipper = require('../models/shipper');
var Shipment = require('../models/shipment');
var Branch = require('../models/branch');
var Truck = require('../models/branch');
var Warehouse = require('../models/warehouse');
var Seal = require('../models/seal');
var ControlShuttle = require('../models/controlshuttle');


models_class = {
    user: User,
    shipper: Shipper,
    shipment: Shipment,
    branch: Branch,
    truck: Truck,
    warehouse: Warehouse,
    seal: Seal,
    controlshuttle: ControlShuttle
}

exports.search = function(req, res){
    var model_name = req.params.model;

    models_class[model_name].
        find(req.body)
        .exec(function(error, results){
            if(error){
                return res.json({error: errorHelper(error)});
            }else{
                return res.json({data: results})
            }
        });
}