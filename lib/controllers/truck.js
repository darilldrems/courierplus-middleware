var Truck = require('../models/truck');
var errorHelper = require('mongoose-error-helper').errorHelper;

exports.listTrucks = function(req, res){
    var page_count = 1;
    var total_per_page = 20;

    Truck.paginate({state: 'active'}, page_count, total_per_page, function(err, pageCount, results, itemCount){
        if(err){
            return res.json({error: errorHelper(err)});
        }
        else{
            return res.json({trucks: results, pagecount: pageCount, itemcount: itemCount});
        }
    });
}

exports.getTruck = function(req, res){
    Truck.findById(req.params.id, function(err, truck){
       if(err){
           return res.json({error: errorHelper(err)});
       }else{
           return res.json({truck: truck});
       }
    });
}

exports.createTruck = function(req, res){
    var truck = new Truck(req.body);
    truck.save(function(err, tr){
       if(err){
           return res.json({error: errorHelper(err)});
       } else{
           return res.json({truck: tr});
       }
    });
}

exports.editTruck = function(req, res){
    Truck.findByIdAndUpdate(req.params.id,{$set: req.body}, function(err, tr){
        if(err){
            return res.json({error: errorHelper(err)});
        }else{
            return res.json({truck: tr});
        }
    });
}

exports.deleteTruck = function(req, res){
    Truck.findByIdAndUpdate(req.body.id, {$set: {state: 'deactivated'}}, function(err, tr){
        if(err){
            return res.json({error: errorHelper(err)});
        }else{
            return res.json({truck: tr});
        }
    })
}