var csv = require('csv-parse');
var fs = require('fs');

var Shipper = require('../models/shipper');
var Shipment = require('../models/shipment');
var User = require('../models/user');

exports.index = function(req, res){
    res.render('upload_form.html', {});
}

exports.shippers = function(req, res){

    var csv_file = req.files.bulk_shippers;
    
    var message = "done!";
    
    fs.createReadStream(csv_file.path)
        .pipe(csv())
        .on('data', function(data){
    
        var shipper = new Shipper();
        
        shipper.name = data[0].trim();
        shipper.tel = data[1].trim();
        shipper.type = data[2].trim();
        shipper.address = data[3].trim();
        shipper.email = data[4].trim();
        shipper.code = data[5].trim();
        
        shipper.save(function(err){
            if(err)
                message = "Error occured. Incorrect csv file";
            
            
        });
        
    })
    .on('finish', function(){
        return res.send(message);
    });

}


exports.shipments = function(req, res){
    var csv_file = req.files.bulk_shipments;
    var message = "done";
    fs.createReadStream(csv_file.path)
        .pipe(csv())
        .on('data', function(data){
        
            Shipper.findOne({name: data[1].trim()}, function(err, shipper){
                if(err){
                        message = "Error occured.check csv file again";
                    return;
                }
                
                if(!shipper){
                        message = "Incorrect shipper name. please check again for match";
                        return;
                }
                
                User.findOne({username: data[11].trim()}, function(err, user){
                    if(err){
                            message = "error occcured please check csv file again";
                            return;
                    }
                    if(!user){
                            message = "error occured please check user column again";
                        return;
                    }
                    
                    var shipment = new Shipment();
                    
                    shipment.hawb = data[0].trim();
                    shipment.shipper = shipper._id;
                    shipment.content = data[2].trim();
                    shipment.status = data[3].trim();
                    shipment.consignee_name = data[4].trim();
                    shipment.consignee_tel = data[5].trim();
                    shipment.consignee_address = data[6].trim();
                    shipment.weight = data[7].trim();
                    shipment.amount = data[8].trim();
                    shipment.payment_mode = data[9].trim();
                    shipment.remark = data[10].trim();
                    shipment.pick_up_by = user._id;
                    shipment.ssr_code = data[12].trim();
                    
                    shipment.save(function(err){
                        if(err){
                            message = "Server error occured. check logs";
                            console.log("Server error:"+JSON.stringify(err));
                        }
                                
                        
                    })
                    
                })
            
            })
    
    
    })
    .on('finish', function(){
        return res.send(message);
    })
    
}
