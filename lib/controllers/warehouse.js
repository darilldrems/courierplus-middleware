var Warehouse = require('../models/warehouse');

exports.createWarehouse = function(req, res){
    var warehouse = new Warehouse(req.body);
    warehouse.save(function(error, ware){
        if(error){
            return res.json({error: error});
        }else{
            return res.json({warehouse: ware});
        }
    });
}

exports.getWarehouse = function(req, res){
    Warehouse.findById(req.params.id, function(error, ware){
        if(err){
            return res.json({error: error});
        }else{
            return res.json({warehouse: ware});
        }
    });
}

exports.editWarehouse = function(req, res){
    Warehouse.findByIdAndUpdate(req.params.id, {$set: req.body}, function(error, ware){
        if(error){
            return res.json({error: error});
        }else{
            return res.json({warehouse: ware});
        }
    });
}

exports.deleteWarehouse = function(req, res){
    Warehouse.findByIdAndUpdate(req.body.id, {$set: {state: 'deactivated'}}, function(error, ware){
        if(error){
            return res.json({error: error});
        }else{
            return res.json({warehouse: ware});
        }
    });
}

exports.listWarehouses = function(req, res){
    var page_count = 1;
    var post_per_page = 20;

    if(req.params.page){
        page_count = parseInt(req.params.page);
//            console.log(req.params.page);
    }

    Warehouse
        .paginate({state: 'active'}, page_count, post_per_page, function(error, pageCount, results, itemCount){
            if(error){
                return res.json({error: errorHelper(error)});
            }else{
                return res.json({warehouses: results, pagecount: pageCount, itemcount: itemCount});
            }
        }, {populate: 'branch'});
}

