var mongoose = require('mongoose');

var authSchema = new mongoose.Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    password: {type: String, required: true}
});

module.exports = mongoose.model('Auth', authSchema);