var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var branchSchema = new mongoose.Schema({
    name: {type: String, required: true, unique: true, lowercase:true},
    location: String,
    state: {type: String, enum:['active', 'deactivated'], default:'active'}
});

branchSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Branch', branchSchema);