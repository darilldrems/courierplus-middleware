var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var controlShuttleSchema = new mongoose.Schema({
    number: {type: String, required: true, unique: true, lowercase:true},
    departure: {type: String, required: true},
    destination: {type: String, required: true},
    shipments: [],
    location:{type: String, required: true},
    state: {type: String, enum: ['active', 'deactivated'], default: 'active'}
});

controlShuttleSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('ControlShuttle', controlShuttleSchema);