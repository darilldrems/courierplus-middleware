/*
*Log Model tracks different actions not tracked on the history page
 */

var mongoose = require('mongoose');
var Q = require('q');
var mongoosePaginate = require('mongoose-paginate');

actions = ['logged in', 'logged out']

var logSchema = new mongoose.Schema({
    user : {type: mongoose.Schema.Types.ObjectId, ref:'User', required: true},
    activity:{type: String, enum:actions, required: true},
    time:{type: Date, default:Date.now()}
});

logSchema.plugin(mongoosePaginate);

logSchema.methods.saveQ = function(){
    var deferred = Q.defer();

    this.save(function(err, obj){
        if(err){
            deferred.reject(err);
        }else{
            deferred.resolve(obj)
        }

    });

    return deferred.promise;
}

module.exports = mongoose.model('Log', logSchema);