var mongoose = require('mongoose');
var Q = require('q');
var mongoosePaginate = require('mongoose-paginate');

var sealHistorySchema = new mongoose.Schema({
    number: {type:String, required:true},

    old_location: String,
    new_location: String,

    old_shipments: [],
    new_shipments: [],

    time: {type: Date, default: Date.now()},

    edited_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

sealHistorySchema.plugin(mongoosePaginate);

sealHistorySchema.methods.addOldValues = function(obj){
    if(obj.number)
        this.number = obj.number

    if(obj.location)
        this.old_location = obj.location

    if(obj.shipments)
        this.old_shipments = obj.old_shipments
}

sealHistorySchema.methods.addNewValues = function(obj){
    if(obj.location)
        this.new_location = obj.location

    if(obj.shipments)
        this.new_shipments = obj.shipments
}

sealHistorySchema.methods.saveQ = function(){
    var deferred = Q.defer();

    this.save(function(err, obj){
        if(err){
            deferred.reject(err);
        }else{
            deferred.resolve(obj);
        }
    })

    return deferred.promise;
}

module.exports = mongoose.model('SealHistory', sealHistorySchema);