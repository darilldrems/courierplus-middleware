var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

status_enum = ['Picked Up','Delivered','Out for delivery', 'Unable to locate the address', 'Illegible address', 'Incorrect address', 'Company not known at the address', 'Shipment refused' ,'Contact person not known at the address', 'Delivery not completed' ,'Not in / closed on delivery - will re-attempt tomorrow', 'Not in / closed on delivery - calling card left', 'Business closed (holiday)', 'Business closed (strike)', 'Future Delivery Requested', 'Consignee moved - new address located', 'Consignee moved - please provide new info.', 'Shipment held for pick-up', 'Shipment destined for area not currently serviced  ', 'Shipment received' ,'Shipment manifested', 'Bags arrived at destination' ,'Onfwd to Cnee (local transp) - Cnee will collect locally', 'Onforwarded to Agent station - delivery in outer location' ,'Onforwarded by post - delivery in outer location', 'Onfwd to subcontractor - POD will not be Available' ,'Pick-up executed', 'Handed over to ( Agent of) Customer of Shipment Picked-up', 'Pickup attempted   ', 'Pickup not attempted', 'Held in Customs - pending clearance', 'Held in Customs - undergoing clearance','Held in Customs - value misdeclared', 'Held in Customs - missing invoice copies', 'Held in Customs - missing original invoice','Held in Customs - invoice shows no value', 'Held in Customs - invoice shows no currency', 'Held in Customs - awaiting info from Consignee', 'Held in Customs - Prohibited items   ', 'Held in Customs','Cleared awaiting payments of duties and taxes by Cnee' ,'Cnee refuses to pay duties /taxes -please advise', 'Given to customs broker', 'Given to consignees Custom BrokerPOD equivalent','SHIPMENT ON HOLD - Value misdeclared', 'SHIPMENT ON HOLD - Missing Invoice Copies', 'SHIPMENT ON HOLD - Missing Original Invoice', 'SHIPMENT ON HOLD - Invoice Shows no Value', 'SHIPMENT ON HOLD - Invoice Shows no Currency','SHIPMENT ON HOLD - AT STATION' ,'Missorted package sent back to export station', 'Rerouted to correct station directly', 'Manifested but not arrived (missing) at import station','Missorted package received', 'Package arrived unmanifested', 'Shipment improperly packed - delay while re-packing', 'Shipment damaged on arrival - attempting delivery', 'Damaged received- pls advise','Multiple package shipment - not all pieces arrived' ,'MISROUTE', 'Flight delay', 'Delayed:offloaded by carrier','DELAYED due to special case:', 'delayed: shipment received after cut-off time', 'Shipment returned to sender as agreed with export station', 'Shipment destroyed upon shippers request', 'Shipment CONFISCATED by Customs','Confirmed lost shipment' ,'COD info for Customer', 'COD info for Agent', 'Accounting info for Customer' ,'Accounting info for Agent', 'New information provided by shipper', 'New documents provided by shipper', 'New information provided by agent', 'Important communication to Shipper','Hard Copy request' ,'1st POD request', '2nd POD request', '3rd POD request','Client Claims non-delivery'];

var shipmentSchema = new mongoose.Schema({
    hawb: {type: String, required: true, unique: true, lowercase:true},
    shipper: {type: mongoose.Schema.Types.ObjectId, ref:'Shipper', required: true},
    content: {type: String, enum:['Document', 'Non Document']},
    status : {type: String, required: true, enum: status_enum},
    consignee_name: {type: String, required: true},
    consignee_tel: {type: String, required: true},
    consignee_address: {type: String, required: true},
    weight: Number,
    amount: Number,
    payment_mode: {type: String, enum:['Cash on delivery', 'POS', 'No Cash on Delivery']},
    remark: String,
    pick_up_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    pick_up_time: {type: Date, default: Date.now()},
    delivered_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    delivery_time: Date,
    warehouse: {type: mongoose.Schema.Types.ObjectId, ref: 'Warehouse'},
    last_modified: Date,
    state: {type: String, enum:['active', 'deactivated'], default:'active'},
    receiver_name:String,
    receiver_tel: String,
    amount_received: Number,
    ssr_code: String
});

shipmentSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Shipment', shipmentSchema);

