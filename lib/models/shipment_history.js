var mongoose = require('mongoose');
var Q = require('q');
var mongoosePaginate = require('mongoose-paginate');

var shipmentHistorySchema = new mongoose.Schema({
    date: {type: Date, default: Date.now()},

    hawb: String,

    old_content: String,
    new_content: String,

    old_status: String,
    new_status: String,

    old_consignee_name: String,
    new_consignee_name: String,

    old_consignee_tel: String,
    new_consignee_tel: String,

    old_consignee_address: String,
    new_consignee_address: String,

    old_weight: Number,
    new_weight: Number,

    old_amount: Number,
    new_amount: Number,

    old_payment_mode: String,
    new_payment_mode: String,

    old_remark: String,
    new_remark: String,

    old_warehouse: {type: mongoose.Schema.Types.ObjectId, ref: 'Warehouse'},
    new_warehouse: {type: mongoose.Schema.Types.ObjectId, ref: 'Warehouse'},

    edited_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

shipmentHistorySchema.plugin(mongoosePaginate);

shipmentHistorySchema.methods.addOldValues = function(obj){

    if(obj.hawb)
        this.hawb = obj.hawb

    if(obj.content)
        this.old_content = obj.content

    if(obj.status)
        this.old_status = obj.status

    if(obj.consignee_name)
        this.old_consignee_name = obj.consignee_name

    if(obj.consignee_tel)
        this.old_consignee_tel = obj.consignee_tel

    if(obj.consignee_address)
        this.old_consignee_address = obj.consignee_address

    if(obj.weight)
        this.old_weight = obj.weight

    if(obj.amount)
        this.old_amount = obj.amount

    if(obj.payment_mode)
        this.old_payment_mode = obj.payment_mode

    if(obj.remark)
        this.old_remark = obj.remark

    if(obj.warehouse)
        this.old_warehouse = obj.warehouse
}


shipmentHistorySchema.methods.addNewValues = function(obj){
    if(obj.hawb)
        this.hawb = obj.hawb

    if(obj.content)
        this.new_content = obj.content

    if(obj.status)
        this.new_status = obj.status

    if(obj.consignee_name)
        this.new_consignee_name = obj.consignee_name

    if(obj.consignee_tel)
        this.new_consignee_tel = obj.consignee_tel

    if(obj.consignee_address)
        this.new_consignee_address = obj.consignee_address

    if(obj.weight)
        this.new_weight = obj.weight

    if(obj.amount)
        this.new_amount = obj.amount

    if(obj.payment_mode)
        this.new_payment_mode = obj.payment_mode

    if(obj.remark)
        this.new_remark = obj.remark

    if(obj.warehouse)
        this.new_warehouse = obj.warehouse
}

shipmentHistorySchema.methods.saveQ = function(){
    var deferred = Q.defer();

    this.save(function(err, obj){
       if(err){
           console.log("error in saveQ"+err);
           deferred.reject({error:err});
       }else{
           console.log("succes in saveQ");
           deferred.resolve({shipment: obj});
       }
    });

    return deferred.promise;
}




module.exports = mongoose.model('ShipmentHistory', shipmentHistorySchema);