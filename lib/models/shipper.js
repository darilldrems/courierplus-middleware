var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var shipperSchema = new mongoose.Schema({
    name: {type: String, unique: true, required: true},
    address: String,
    tel: String,
    type: {type: String, enum:['Credit', 'Non Credit']},
    email: {type: String, unique: true},
    state: {type: String, enum: ['active', 'deactivated'], default:'active'},
    createdOn: {type: Date, default: Date.now()},
    code: String
});

shipperSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Shipper', shipperSchema);
