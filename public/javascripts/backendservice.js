var backEndService = angular.module('BackEndService', ['MyConstants', 'SessionService']);

backEndService.factory('AuthService', ['$http', 'backendUrl', 'Session', function($http, backendUrl, Session){
    var authService = {};

    authService.login = function(credentials){
        return $http
            .post(backendUrl.login, credentials)
            .then(function(res){
//                console.log('entered backend service');
                if(res.data.user){
                    Session.create(res.data.user);
                }
                return res.data;
            });

    }

    return authService;
}]);

backEndService.factory('Api', ['$http', 'backendUrl', function($http, backendUrl){
    var apiService = {}

    apiService.listUsers = function(page_number){
        if(!page_number)
            page_number = 1;
        return $http.get(backendUrl.usersList+'?page='+page_number).then(function(res){
            return res.data;
        })
    }

    apiService.lists = function(page_number, model, options){
        if(!options)
            options = {}
        return $http.get(backendUrl.baseUrl+model+'?page='+page_number, {params:options}).then(function(res){
            return res.data;
        })
    }



    apiService.delete = function(model, id){
        console.log('what is id:'+id);
        return $http.post(backendUrl.baseUrl+model+'/delete', {id:id}).then(function(res){
            console.log('delete return data: '+res.data);
            return res.data;
        })
    }

    apiService.create = function(model, body){
        console.log('body is: '+body);
        return $http.post(backendUrl.baseUrl+model+'/new', body).then(function(res){
            return res.data;
        })
    }

    apiService.register = function(body){
        return $http.post(backendUrl.baseUrl+'register', body).then(function(res){
            console.log(res.status);
            return res.data;
        });
    }

    apiService.get = function(model_name, id){
        return $http.get(backendUrl.baseUrl+model_name+'/'+id).then(function(res){
            return res.data;
        });
    }

    apiService.update = function(model_name, id, data){
        return $http.post(backendUrl.baseUrl+model_name+'/edit/'+id, data).then(function(res){
           return res.data;
        });
    }

    apiService.dashboard = function(){
        return $http.get(backendUrl.baseUrl+'dashboard').then(function(res){
            return res.data;
        });
    }

    apiService.history = function(model_name, filter){
        console.log(filter);
        return $http.get(backendUrl.baseUrl+'history/'+model_name, {params:filter}).then(function(res){
            return res.data;
        });
    }

    return apiService;
}])