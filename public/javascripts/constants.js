var base_url = '/backend/api/';
angular
    .module('MyConstants', [])
    .constant('backendUrl', {
        login: base_url + 'login',
        usersList: base_url + 'users',
        baseUrl: base_url
    })
    .constant('Events', {
        loginSuccess: 'login-successful',
        loginFailure: 'loginFailed'
    })
    .constant('UserRoles', {
        roles: ['control', 'personel', 'administrator']
    })
    .constant('DeliveryStatus', {
       status: ['Picked Up', 'Out for delivery', 'Delivered', 'Consignee moved']
    })
    .constant('Location', {
        cities: ['Abuja', 'Lagos', 'Lokoja', 'Benue', 'Ibadan']
    });