(function(){

//    var dependency = ['$rootScope'];

    var exportTable = function(){

        var link = function($scope, elm, attr){


            $scope.$on('export-pdf', function(e, d){
//
                elm.tableExport({type:'pdf', escape:'false'});
            });

            $scope.$on('export-excel', function(e, d){
                elm.tableExport({type:'excel', escape:false});
            });

            $scope.$on('export-doc', function(e, d){
                elm.tableExport({type: 'doc', escape:false});
            });

        }

        return {
            restrict: 'C',
            link: link
        }
    }


    var blockUi = function(){

        var link = function($scope, elm, attr){
            $scope.$on('loading', function(data){
                    console.log('event loadding broadcasted');
                    angular.element.blockUI({ css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    } });

//                    setTimeout($.unblockUI, 2000);

            });

            $scope.$on('loading-done', function(data){
                console.log('event loading-done broadcasted');
                angular.element.unblockUI();
            })
        }



        return {
            restrict: 'E',
            link: link
        }
    }



    angular
        .module('CustomDirectives', [])
        .directive('exportTable', exportTable)
        .directive('blockUi', blockUi)
})();