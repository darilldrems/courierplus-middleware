angular
    .module('SessionService', [])
    .factory('Session', function(){
        this.create = function(sess){
            this.id = sess._id;
            this.username = sess.username;
            this.role = sess.role;
            this.email = sess.email;
            this.first_name = sess.first_name;
            this.last_name = sess.last_name;
//            console.log('inside sess'+sess.username);
        }

        this.destroy = function(){
            this.id = null;
            this.username = null;
            this.role = null;
            this.email = null;
            this.first_name = null;
            this.last_name = null;
        }

        return this;
    })
